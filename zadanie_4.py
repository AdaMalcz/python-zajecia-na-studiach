# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 23:24:26 2018

@author: Rege
"""

#program oblicza ciąg n liczb Fibonacciego począwszy od liczb x i y
#na podstawie własnej funkcji o argumentach n, x, y

#Klasyczny ciąg Fibonacciego definiujemy następująco:
#pierwszy wyraz jest równy 0, drugi jest równy 1, 
#a każdy następny jest sumą dwóch poprzednich.

#polecane argumenty dla uzyskania wyniku to [0 1 5] oraz [1 2 5] (inne powiadomienie)
#polecane argumenty dla wywołania błędu to [0 1 1] oraz [0 1 -1] oraz [0 1 1.1]

#zaimportuj zmienne systemowe i biblioteke math
import sys
import math

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=4:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 3 argumenty.')

#IDIOTOODPORNOŚĆ: n jest liczba calkowita
if float(sys.argv[3])-math.floor(float(sys.argv[3]))>0:
    sys.exit('BLAD: podana liczba elementow ciagu nie jest liczba calkowita!') #zatrzymaj program w tym miejscu i wyswietl błąd
    
x=float(sys.argv[1])
y=float(sys.argv[2])
n=int(sys.argv[3])

print("\n")

#IDIOTOODPORNOŚĆ: ciag zawiera conajmniej 3 wyrazy, n jest nieujemne
if n<0:
    sys.exit('BLAD: ilosc wyrazow musi byc liczba dodatnia!') #zatrzymaj program w tym miejscu i wyswietl błąd
elif n<=2:
    sys.exit('BLAD: ciag Fibonacciego ma sens jesli zawiera conajmniej 3 wyrazy. Wprowadz liczbe wyrazow 3 lub wieksza!') #zatrzymaj program w tym miejscu i wyswietl błąd

#inne powiadomienie w zależnosci od argumentów wejsciowych
print('Program utworzy ciag Fibonacciego zawierajacy',n,'wyrazow. Pierwsze dwa wyrazy ciagu to',x,'i',y,', a kolejne sa suma dwoch poprzednich.')
if x==0:
    if y==1:
        print('Jest to klasyczny ciag Fibonacciego.',"\n")
    else:
        print('Jest to nietypowy ciag. Klasyczny ciag Fibonacciego zaczyna sie od liczb 0 i 1.',"\n")
else:
    print('Jest to nietypowy ciag. Klasyczny ciag Fibonacciego zaczyna sie od liczb 0 i 1.',"\n")

#zdefiniuj funkcję Fibonacci(liczba1, liczba2, długosć ciągu)
def Fibonacci(x,y,n):
    fib=[x,y]
    l=0
    for i in range(2,n):    #zacznij iterować od 2 (miejsca 0 i 1 ciągu narzucone z góry)
        l=fib[-1]+fib[-2]   #do pierwszej od końca liczby w ciągu dodaj drugą od końca liczbę w ciagu
        fib.append(l)       #dopisz wynik do końca ciągu
    return fib              #zwróc cały ciąg liczb

print('Wygenerowany ciag Fibonacciego to', Fibonacci(x,y,n))
