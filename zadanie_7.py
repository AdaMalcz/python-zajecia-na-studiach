# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 17:38:26 2018

@author: Rege
"""

#program zlicza  samogłoski w wyrazie podanym przez użytkownika oraz dopisuje 
#wynik  na  końcu  pliku  o  nazwie  zadanej  jako argument wywołania programu 
#w formacie: wyraz tabulator liczba_samoglosek

import sys

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=2:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')
    
nazwa=sys.argv[1]

#SAMOGŁOSKI: [a] [e] [i] [o] [u] [y]
print("\n")
print('Program zliczy sylaby w podanym wyrazie. Znane sylaby to: [a] [e] [i] [o] [u] [y]',"\n")

#użytkownik podaje wyraz na standardowym wejsciu
wyraz = input("Podaj wyraz:")

#przypisanie ilosci samogłosek do każdej z występujących samogłosek
a=wyraz.count('a')
e=wyraz.count('e')
i=wyraz.count('i')
o=wyraz.count('o')
u=wyraz.count('u')
y=wyraz.count('y')

#podanie ilosci poszczegolnych samogłosek oraz ich sumy
print("\n")
print('Podano wyraz:',wyraz,"\n")
if a>0:
    print('W wyrazie zanjduje sie',a,'samoglosek a.')
if e>0:
    print('W wyrazie zanjduje sie',e,'samoglosek e.')
if i>0:
    print('W wyrazie zanjduje sie',i,'samoglosek i.')
if o>0:
    print('W wyrazie zanjduje sie',o,'samoglosek o.')
if u>0:
    print('W wyrazie zanjduje sie',u,'samoglosek u.')
if y>0:
    print('W wyrazie zanjduje sie',y,'samoglosek y.')
suma=a+e+i+o+u+y
print("\n")
print('W sumie w wyrazie znajduje sie',suma,'samoglosek.',"\n")

#dopisanie informacji do pliku
lista=[wyraz,"\t",str(suma),"\n"]
plik=open(nazwa,'a') #a - append
plik.writelines(lista)
plik.close()

print('Wyraz oraz sume zliczonych samoglosek dopisano na koniec pliku', nazwa, 'w folderze zawierajacym kod programu')
