# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 15:20:36 2018

@author: Rege
"""

#program służy jedynie do wywietlania informacji o poszczególnych programach
#z listy. Zawiera opisy programów, sposoby ich wywołania oraz sugestie
#dotyczące argumentów wejciowych programów w celu przetesgtowania każdej jego
#funkcji
import sys

print("\n")
print('Repozytorium zawiera programy realizujace wszystkie zadania z listy 1 dla Pythona.')
print('Autorem repozytorium jest Adam Malczewski (208470)')
print('Grupa:',"\t",'sroda 15:15',"\n")

argumenty=len(sys.argv)

if argumenty == 1:
    
    print('Zadanie 1:')
    print('Opis:')
    print("\t",'Program oblicza i wyswietla pole trojkata o dlugosci zadanej przez uzytkownika jako argumenty wywolania programu.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_1.py a b c',"\t",'gdzie: a, b i c to dlugosci kolejnych bokow trojkata',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program nie pozwoli na obliczenie pola trojkata ktorego nie da sie stworzyc (dlugosci bokow musza byc odpowiednie)')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [2 2 3] spowoduje obliczenie pola trojkata. Istnieje trojkat o takich bokach')
    print("\t",'- [1 2 3] spowoduje wyswietlenie bledu. Trojkat o takich bokach nie istnieje')
    print("\t",'- [2 3] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   sprawdzanie czy trojkat jest rownoboczny/rownoramienny/prostokatny
    #   liczenie katow w trojkacie
    
    print("\n")
    
    print('Zadanie 2:')
    print('Opis:')
    print("\t",'Program wykonuje dzielenie dwoch liczb calkowitych podanych przez uzytkownika oraz wyswietla wynik z dokladnoscia do 4 miejsc po przecinku.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_2.py x y',"\t",'gdzie: x i y to liczby dzielone przez siebie (x/y)',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program nie pozwoli na dzielenie przez zero')
    print("\t",'- program zaokragli liczby niecalkowite podane przez uzytkownika (i wyswietli komunikat)')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [10 6] spowoduje wyswietlenie dwoch roznych wynikow w zaleznosci od przyjetej metody okreslenia dokladnosci')
    print("\t",'- [10 6.1] spowoduje zaokraglenie y do liczby calkowitej i wyswietli komunikat')
    print("\t",'- [10 0] spowoduje wyswietlenie bledu. Nie mozna dzielic przez zero')
    print("\t",'- [10] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   użytkownik okresla dokladnosc oraz metode wyswietlania wyniku
    
    print("\n")
    
    print('Zadanie 3:')
    print('Opis:')
    print("\t",'Program oblicza sume kwadratow liczb od 1 do n (podane przez uzytkownika).',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_3.py n',"\t",'gdzie: n to ostatnia liczba podniesiona do kwadratu',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program nie pozwoli na wprowadzenie liczby mniejszej od 1 (pierwszy wyraz ciagu)')
    print("\t",'- program nie pozwoli podac liczby niecalkowitej')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [4] spowoduje wyswietlenie wyniku')
    print("\t",'- [0] spowoduje wyswietlenie bledu - liczba mniejsza od pierwszego wyrazu ciagu')
    print("\t",'- [1.1] spowoduje wyswietlenie bledu - liczba jest niecalkowita')
    print("\t",'- [4 4] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   chyba nic więcej
    
    print("\n")
    
    print('Zadanie 4:')
    print('Opis:')
    print("\t",'Program oblicza ciag n liczb Fibonacciego poczawszy od liczb x i y.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_4.py x y n',"\t",'gdzie: x to pierwszy wyraz ciagu, y to drugi wyraz ciagu, n to ilosc wyrazow w ciagu',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program nie pozwoli na liczby wyrazow ciagu mniejszej od 3')
    print("\t",'- program nie pozwoli podac liczby ujemnej badz niecalkowitej jako liczbe wyrazow ciagu')
    print("\t",'- program okresli czy podany ciag jest klasycznym czy nietypowym ciagiem Fibonacciego')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [0 1 5] spowoduje wyswietlenie wyniku oraz komunikatu o klasycznym ciagu')
    print("\t",'- [1 2 5] spowoduje wyswietlenie wyniku oraz komunikatu o nietypowym ciagu')
    print("\t",'- [0 1 1] spowoduje wyswietlenie bledu - ciag musi sie skladac conajmniej z 3 wyrazow')
    print("\t",'- [0 1 -1] spowoduje wyswietlenie bledu - liczba wyrazow ciagu jest ujemna')
    print("\t",'- [0 1 1.1] spowoduje wyswietlenie bledu - liczba wyrazow ciagu jest niecalkowita')
    print("\t",'- [0 1 5 5] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   wygenerowanie wykresu ciagu Fibonacciego (fajna petelka) - nie do zrobienia przy wywolywaniu w konsoli
    
    print("\n")
    
    print('Zadanie 5:')
    print('Opis:')
    print("\t",'Program tworzy tablice dwuwymiarowa zawierajaca tabliczke mnozenia liczb o wymiarze podanym przez uzytkownika.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_5.py n',"\t",'gdzie: n to wymiar tablicy',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program nie pozwoli na wprowadzenie ujemnej lub niecalkowitej liczby wymiaru tablicy')
    print("\t",'- program wyswietli tablice w sposob czytelny dla uzytkownika (rowne kolumny)')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [10] spowoduje wyswietlenie wyniku w postaci klasycznej tabliczki mnozenia')
    print("\t",'- [-10] spowoduje wyswietlenie bledu - wymiar tablicy musi byc dodatni')
    print("\t",'- [10.1] spowoduje wyswietlenie bledu - wymiar tablicy jest liczba niecalkowita')
    print("\t",'- [10 10] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   chyba nic więcej
    
    print("\n")
    
    print('Zadanie 6:')
    print('Opis:')
    print("\t",'Program oblicza n elementow (podane przez uzytkownika) ciagu geometrycznego okreslonego wzorem: a[n+1] = a[n]*q',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_6.py a0 q n',"\t",'gdzie: a0 to pierwszy wyraz ciagu, q to iloraz ciagu, n to ilosc wyrazow w ciagu',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program wyswietli odpowiedni komunikat jesli wprowadzone argumenty spowoduja powstanie ciagu o identycznych wyrazach')
    print("\t",'- program wyswietli blad jesli podana ilosc wyrazow ciagu bedzie liczba niecalkowita badz nie bedzie liczba dodatnia')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [1 2 10] spowoduje uzyskanie wyniku - zwroci ciag roznych liczb')
    print("\t",'- [0 2 10] spowoduje wyswietlenie powiadomienia - wszystkie wyrazy ciagu beda rowne 0')
    print("\t",'- [1 1 10] spowoduje wyswietlenie powiadomienia - wszystkie wyrazy ciagu beda takie same')
    print("\t",'- [1 0 10] spowoduje wyswietlenie powiadomienia - wszystkie wyrazy ciagu beda rowne 0')
    print("\t",'- [1 2 0] spowoduje wyswietlenie bledu - ilosc wyrazow ciagu nie jest liczba dodatnia')
    print("\t",'- [1 2 -1] spowoduje wyswietlenie bledu - ilosc wyrazow ciagu nie jest liczba dodatnia')
    print("\t",'- [1 2 1.3] spowoduje wyswietlenie bledu - ilosc wyrazow ciagu nie jest liczba calkowita')
    print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   chyba nic więcej
    
    print("\n")
    
    print('Zadanie 7:')
    print('Opis:')
    print("\t",'Program zlicza  samogloski w wyrazie podanym przez uzytkownika oraz dopisuje wynik na koncu pliku o nazwie zadanej jako argument wywolania programu w formacie:')
    print("\t",'"wyraz" "tabulator" "liczba_samoglosek". Plik (o ile nie istnieje) zostanie utworzony w folderze w ktorym znajduje sie sam program.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_7.py nazwa pliku',"\t",'UWAGA: nazwa pliku musi zawierac rozszerzenie',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program wyswietli podane przez uzytkownika slowo, liczbe poszczegolnych samoglosek oraz ich sume')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [zadanie7.txt] spowoduje dopisanie linijki do konca pliku (plik zostanie utworzony jesli nie istnieje)')
    print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   chyba nic więcej
    
    print("\n")
    
    print('Zadanie 7 dodatkowe:')
    print('Opis:')
    print("\t",'Program zlicza  samogloski w wyrazach podawanych przez uzytkownika w petli oraz dopisuje wynik na koncu pliku o nazwie zadanej jako argument wywolania programu w formacie:')
    print("\t",'"wyraz" "tabulator" "liczba_samoglosek". Plik (o ile nie istnieje) zostanie utworzony w folderze w ktorym znajduje sie sam program.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_7_dodatkowe.py nazwa pliku',"\t",'UWAGA: nazwa pliku musi zawierac rozszerzenie',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program wyswietli podane przez uzytkownika slowo, liczbe poszczegolnych samoglosek oraz ich sume')
    print("\t",'- program poza komenda wyjscia z programu zawiera rowniez komendy wyswietlania oraz czyszczenia pliku')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [zadanie7d.txt] spowoduje dopisanie linijki do konca pliku (plik zostanie utworzony jesli nie istnieje)')
    print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   dodać różne komendy, np. wyswietl konkretna linijke z pliku
    
    print("\n")
    
    print('Zadanie 8:')
    print('Opis:')
    print("\t",'Program wczytuje plik tekstowy o nazwie podanej przez uzytkownika (np. utworzony w zadaniu 7) oraz wyswietla liste wyrazow posortowanych po liczbie samoglosek.',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_8.py nazwa pliku',"\t",'UWAGA: plik musi znajdowac sie w folderze z programem, a nazwa pliku musi zawierac rozszerzenie',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program wyswietli zawartosc pliku oraz ilosc wyrazow')
    print("\t",'- program wyswieli blad jesli nie uda mu sie otworzyc pliku (np. kiedy plik nie istnieje lub uzytkownik poda zla nazwe)')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [zadanie_8.txt] spowoduje wyswietlenie posortowanych wyrazow ze wczesniej utworzonego pliku')
    print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   sortowanie malejąco
    #   inny klucz sortowania 
    
    print("\n")
    
    print('Zadanie 9:')
    print('Opis:')
    print("\t",'Program wczytuje dwa pliki tekstowe o nazwach podanych przez uzytkownika oraz wyswietla wyrazy ktore znajduje sie w obu plikach (bez uwzglednienia wielkosci liter).',"\n")
    print('Uruchomienie:')
    print("\t",'python zadanie_9.py "nazwa pliku" "nazwa pliku"',"\t",'UWAGA: pliki musza znajdowac sie w folderze z programem, a ich nazwy musza zawierac rozszerzenia',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- jesli wyrazy obecne w obu plikach roznia sie wielkoscia liter program o tym poinformuje')
    print("\t",'- program wyswieli blad jesli nie uda mu sie otworzyc pliku (np. kiedy plik nie istnieje lub uzytkownik poda zla nazwe)')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [tekst1.txt tekst2.txt] spowoduje wyswietlenie wyrazow ktore obecne sa w obu plikach')
    print("\t",'- [1 2 3] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   wyswietlic ilosc powtorzen danego slowa dla danego pliku zamiast powielac slowo na liscie
    #   nie brac pod uwage znakow interpunkcyjnych (usunac je ze zmiennych uzywanych do porownania)

elif argumenty == 2:
    zadanie=sys.argv[1]
    if zadanie == "1":
        print('Zadanie 1:')
        print('Opis:')
        print("\t",'Program oblicza i wyswietla pole trojkata o dlugosci zadanej przez uzytkownika jako argumenty wywolania programu.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_1.py a b c',"\t",'gdzie: a, b i c to dlugosci kolejnych bokow trojkata',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program nie pozwoli na obliczenie pola trojkata ktorego nie da sie stworzyc (dlugosci bokow musza byc odpowiednie)')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [2 2 3] spowoduje obliczenie pola trojkata. Istnieje trojkat o takich bokach')
        print("\t",'- [1 2 3] spowoduje wyswietlenie bledu. Trojkat o takich bokach nie istnieje')
        print("\t",'- [2 3] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "2":
        print('Zadanie 2:')
        print('Opis:')
        print("\t",'Program wykonuje dzielenie dwoch liczb calkowitych podanych przez uzytkownika oraz wyswietla wynik z dokladnoscia do 4 miejsc po przecinku.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_2.py x y',"\t",'gdzie: x i y to liczby dzielone przez siebie (x/y)',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program nie pozwoli na dzielenie przez zero')
        print("\t",'- program zaokragli liczby niecalkowite podane przez uzytkownika (i wyswietli komunikat)')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [10 6] spowoduje wyswietlenie dwoch roznych wynikow w zaleznosci od przyjetej metody okreslenia dokladnosci')
        print("\t",'- [10 6.1] spowoduje zaokraglenie y do liczby calkowitej i wyswietli komunikat')
        print("\t",'- [10 0] spowoduje wyswietlenie bledu. Nie mozna dzielic przez zero')
        print("\t",'- [10] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "3":
        print('Zadanie 3:')
        print('Opis:')
        print("\t",'Program oblicza sume kwadratow liczb od 1 do n (podane przez uzytkownika).',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_3.py n',"\t",'gdzie: n to ostatnia liczba podniesiona do kwadratu',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program nie pozwoli na wprowadzenie liczby mniejszej od 1 (pierwszy wyraz ciagu)')
        print("\t",'- program nie pozwoli podac liczby niecalkowitej')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [4] spowoduje wyswietlenie wyniku')
        print("\t",'- [0] spowoduje wyswietlenie bledu - liczba mniejsza od pierwszego wyrazu ciagu')
        print("\t",'- [1.1] spowoduje wyswietlenie bledu - liczba jest niecalkowita')
        print("\t",'- [4 4] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "4":
        print('Zadanie 4:')
        print('Opis:')
        print("\t",'Program oblicza ciag n liczb Fibonacciego poczawszy od liczb x i y.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_4.py x y n',"\t",'gdzie: x to pierwszy wyraz ciagu, y to drugi wyraz ciagu, n to ilosc wyrazow w ciagu',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program nie pozwoli na liczby wyrazow ciagu mniejszej od 3')
        print("\t",'- program nie pozwoli podac liczby ujemnej badz niecalkowitej jako liczbe wyrazow ciagu')
        print("\t",'- program okresli czy podany ciag jest klasycznym czy nietypowym ciagiem Fibonacciego')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [0 1 5] spowoduje wyswietlenie wyniku oraz komunikatu o klasycznym ciagu')
        print("\t",'- [1 2 5] spowoduje wyswietlenie wyniku oraz komunikatu o nietypowym ciagu')
        print("\t",'- [0 1 1] spowoduje wyswietlenie bledu - ciag musi sie skladac conajmniej z 3 wyrazow')
        print("\t",'- [0 1 -1] spowoduje wyswietlenie bledu - liczba wyrazow ciagu jest ujemna')
        print("\t",'- [0 1 1.1] spowoduje wyswietlenie bledu - liczba wyrazow ciagu jest niecalkowita')
        print("\t",'- [0 1 5 5] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "5":
        print('Zadanie 5:')
        print('Opis:')
        print("\t",'Program tworzy tablice dwuwymiarowa zawierajaca tabliczke mnozenia liczb o wymiarze podanym przez uzytkownika.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_5.py n',"\t",'gdzie: n to wymiar tablicy',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program nie pozwoli na wprowadzenie ujemnej lub niecalkowitej liczby wymiaru tablicy')
        print("\t",'- program wyswietli tablice w sposob czytelny dla uzytkownika (rowne kolumny)')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [10] spowoduje wyswietlenie wyniku w postaci klasycznej tabliczki mnozenia')
        print("\t",'- [-10] spowoduje wyswietlenie bledu - wymiar tablicy musi byc dodatni')
        print("\t",'- [10.1] spowoduje wyswietlenie bledu - wymiar tablicy jest liczba niecalkowita')
        print("\t",'- [10 10] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "6":
        print('Zadanie 6:')
        print('Opis:')
        print("\t",'Program oblicza n elementow (podane przez uzytkownika) ciagu geometrycznego okreslonego wzorem: a[n+1] = a[n]*q',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_6.py a0 q n',"\t",'gdzie: a0 to pierwszy wyraz ciagu, q to iloraz ciagu, n to ilosc wyrazow w ciagu',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program wyswietli odpowiedni komunikat jesli wprowadzone argumenty spowoduja powstanie ciagu o identycznych wyrazach')
        print("\t",'- program wyswietli blad jesli podana ilosc wyrazow ciagu bedzie liczba niecalkowita badz nie bedzie liczba dodatnia')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [1 2 10] spowoduje uzyskanie wyniku - zwroci ciag roznych liczb')
        print("\t",'- [0 2 10] spowoduje wyswietlenie powiadomienia - wszystkie wyrazy ciagu beda rowne 0')
        print("\t",'- [1 1 10] spowoduje wyswietlenie powiadomienia - wszystkie wyrazy ciagu beda takie same')
        print("\t",'- [1 0 10] spowoduje wyswietlenie powiadomienia - wszystkie wyrazy ciagu beda rowne 0')
        print("\t",'- [1 2 0] spowoduje wyswietlenie bledu - ilosc wyrazow ciagu nie jest liczba dodatnia')
        print("\t",'- [1 2 -1] spowoduje wyswietlenie bledu - ilosc wyrazow ciagu nie jest liczba dodatnia')
        print("\t",'- [1 2 1.3] spowoduje wyswietlenie bledu - ilosc wyrazow ciagu nie jest liczba calkowita')
        print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "7":
        print('Zadanie 7 dodatkowe:')
        print('Opis:')
        print("\t",'Program zlicza  samogloski w wyrazach podawanych przez uzytkownika w petli oraz dopisuje wynik na koncu pliku o nazwie zadanej jako argument wywolania programu w formacie:')
        print("\t",'"wyraz" "tabulator" "liczba_samoglosek". Plik (o ile nie istnieje) zostanie utworzony w folderze w ktorym znajduje sie sam program.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_7_dodatkowe.py nazwa pliku',"\t",'UWAGA: nazwa pliku musi zawierac rozszerzenie',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program wyswietli podane przez uzytkownika slowo, liczbe poszczegolnych samoglosek oraz ich sume')
        print("\t",'- program poza komenda wyjscia z programu zawiera rowniez komendy wyswietlania oraz czyszczenia pliku')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [zadanie7d.txt] spowoduje dopisanie linijki do konca pliku (plik zostanie utworzony jesli nie istnieje)')
        print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "7*":
        print('Zadanie 7 dodatkowe:')
        print('Opis:')
        print("\t",'Program zlicza  samogloski w wyrazach podawanych przez uzytkownika w petli oraz dopisuje wynik na koncu pliku o nazwie zadanej jako argument wywolania programu w formacie:')
        print("\t",'"wyraz" "tabulator" "liczba_samoglosek". Plik (o ile nie istnieje) zostanie utworzony w folderze w ktorym znajduje sie sam program.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_7_dodatkowe.py nazwa pliku',"\t",'UWAGA: nazwa pliku musi zawierac rozszerzenie',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program wyswietli podane przez uzytkownika slowo, liczbe poszczegolnych samoglosek oraz ich sume')
        print("\t",'- program poza komenda wyjscia z programu zawiera rowniez komendy wyswietlania oraz czyszczenia pliku')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [zadanie7d.txt] spowoduje dopisanie linijki do konca pliku (plik zostanie utworzony jesli nie istnieje)')
        print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "8":
        print('Zadanie 8:')
        print('Opis:')
        print("\t",'Program wczytuje plik tekstowy o nazwie podanej przez uzytkownika (np. utworzony w zadaniu 7) oraz wyswietla liste wyrazow posortowanych po liczbie samoglosek.',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_8.py nazwa pliku',"\t",'UWAGA: plik musi znajdowac sie w folderze z programem, a nazwa pliku musi zawierac rozszerzenie',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program wyswietli zawartosc pliku oraz ilosc wyrazow')
        print("\t",'- program wyswieli blad jesli nie uda mu sie otworzyc pliku (np. kiedy plik nie istnieje lub uzytkownik poda zla nazwe)')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [zadanie_8.txt] spowoduje wyswietlenie posortowanych wyrazow ze wczesniej utworzonego pliku')
        print("\t",'- [1 2] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    elif zadanie == "9":
        print('Zadanie 9:')
        print('Opis:')
        print("\t",'Program wczytuje dwa pliki tekstowe o nazwach podanych przez uzytkownika oraz wyswietla wyrazy ktore znajduje sie w obu plikach (bez uwzglednienia wielkosci liter).',"\n")
        print('Uruchomienie:')
        print("\t",'python zadanie_9.py "nazwa pliku" "nazwa pliku"',"\t",'UWAGA: pliki musza znajdowac sie w folderze z programem, a ich nazwy musza zawierac rozszerzenia',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- jesli wyrazy obecne w obu plikach roznia sie wielkoscia liter program o tym poinformuje')
        print("\t",'- program wyswieli blad jesli nie uda mu sie otworzyc pliku (np. kiedy plik nie istnieje lub uzytkownik poda zla nazwe)')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [tekst1.txt tekst2.txt] spowoduje wyswietlenie wyrazow ktore obecne sa w obu plikach')
        print("\t",'- [1 2 3] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    else:
        sys.exit('Nie ma takiego zadania.')

else:
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')
    
 