# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 16:09:22 2018

@author: Rege
"""
#program wykonuje dzielenie dwóch liczb całkowitych podanych przez użytkownika 
#i podaje wynik z dokladnosica do 4 miejsc po przecinku
#polecane argumenty dla uzyskania wyniku to [10 6]
#polecane argumenty dla wywołania błędu to [10 0]

#zaimportuj zmienne systemowe oraz bibliotekę math
import sys 
import math

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=3:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 2 argumenty.')

a=float(sys.argv[1])
b=float(sys.argv[2])

#sprawdz czy jako argumenty podano liczby calkowite
if a-math.floor(a)>0:
    print("\n")
    print('Podano liczbe niecalkowita',a,'- zostala przekonwertowana do odpowiedniego formatu:',int(round(a,0)))
if float(b)-math.floor(b)>0:
    print("\n")
    print('Podano liczbe niecalkowita',sys.argv[2],'- zostala przekonwertowana do odpowiedniego formatu:',int(round(b,0)))

#przypisz argumenty wywołania do zmiennych
a=int(round(a,0)) 
b=int(round(b,0))

#IDIOTOODPORNOSC: dzielenie przez zero
if b==0:
    print("\n")
    sys.exit('Pamietaj cholero - nie dziel przez zero!')

#wykonaj dzielenie
c=a/b

print("\n")

#wyswietl wynik z dokładnoscia do 4 miejsc po przecinku
print('Program podaje wynik z dokladnoscia do 4 miejsc po przecinku na dwa sposoby - poprzez zaokraglenie oraz uciecie reszty.',"\n")
    
    #zaokrąglenie z wykorzystaniem funkcji round(liczba, ilosć miejsc po przecinku)
w1=round(c,4)
print('Wynik dzielenia ',a,' przez ',b,' z dokladnoscia do 4 miejsc po przecinku przy wykorzystaniu funkcji round wynosi ',w1,', jest to zaokraglenie wyniku',c )

    #zdefiniowanie własnej funkcji dokladnosc(liczba, ilosc miejs po przecinku)
def dokladnosc(l, n):
    return math.floor(l * 10 ** n) / 10 ** n #floor zwraca największego int'a z argumentu ; ** to potęgowanie

    #wywołanie własnej funkcji
w2=dokladnosc(c,4)
print('Wynik dzielenia ',a,' przez ',b,' z dokladnoscia do 4 miejsc po przecinku przy wykorzystaniu wlasnej funkcji wynosi ',w2,', jest to uciecie reszty wyniku',c )