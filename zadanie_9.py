# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 23:11:12 2018

@author: Rege
"""

#program wczytuje dwa pliki tekstowe (argumenty wywołania) zawierające wyrazy
#oddzielone białymi znakami i znajduje wyrazy które pojawiają się w obu plikach
#obsługiwane są tylko litery (nierozróźnialne na duże i małe) bez polskich znaków

#zaimportuj biblioteke i przypisz argumenty wywołania do zmiennych
import sys

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=3:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 2 argumenty.')

plik1=sys.argv[1]   #nazwa pierwszego pliku
plik2=sys.argv[2]   #nazwa drugiego pliku

#pobierz dane z plików
try:                        #spróbuj otworzyć plik
    wyrazy1=open(plik1,'r')
except:                     #jesli nie uda ci się otworzyć pliku wyswietl błąd i zamknij program
    print("\n")
    print(plik1)
    sys.exit('Nie mozna otworzyc pliku. Upewnij sie ze wpisano poprawna nazwe pliku wraz z jego rozszerzeniem oraz ze plik znajduje sie w folderze zawierajacym kod programu.') #zatrzymaj program w tym miejscu i wyswietl błąd
lista1=wyrazy1.read()
wyrazy1.close()

try:                        #spróbuj otworzyć plik
    wyrazy2=open(plik2,'r')
except:                     #jesli nie uda ci się otworzyć pliku wyswietl błąd i zamknij program
    print("\n")
    print(plik2)
    sys.exit('Nie mozna otworzyc pliku. Upewnij sie ze wpisano poprawna nazwe pliku wraz z jego rozszerzeniem oraz ze plik znajduje sie w folderze zawierajacym kod programu.') #zatrzymaj program w tym miejscu i wyswietl błąd
lista2=wyrazy2.read()
wyrazy2.close()

#wyswietl komunikat o funkcji programu
print("\n")
print('Program pobral dane z plikow',plik1,'oraz',plik2,'.',"\n")
print("\t",plik1,'zawiera nastepujacy tekst:')
print(lista1,"\n")
print("\t",plik2,'zawiera nastepujacy tekst:')
print(lista2,"\n")

#rozdziel wyrazy z pliku poprzez tabulatory i zapisz do zmiennej typu lista
wyrazy1=lista1.split()
wyrazy2=lista2.split()

#zapisz wszystkie wyrazy do analogicznej zmiennej z małych liter 
xwyrazy1=lista1.lower().split()
xwyrazy2=lista2.lower().split()

#zlicz wyrazy z każdego pliku
a=len(wyrazy1)
b=len(wyrazy2)

#wyswietl komunikat o funkcji programu
print('W obu plikach znaleziono nastepujace wyrazy ktore sie pokrywaja:')

#dla każdego wyrazu z pierwszego pliku sprawdź czy którys z wyrazów w drugim jest taki sam
for i in range(a):
    for j in range(b):
        if xwyrazy1[i]==xwyrazy2[j]:    #jesli trafiono na takie same wyrazy to:
            if wyrazy1[i]==wyrazy2[j]:  #jesli sa zapisane z takich samych liter 
                print('-',wyrazy1[i])       #   -wyswietl ten wyraz
            else:                       #w przeciwnym wypadku wyswietl ten wyraz z odpowiednim komentarzem
                print('-',wyrazy1[i],"\t",'(w pliku',plik1,'jako [',wyrazy1[i],'], a w pliku',plik2,'jako [',wyrazy2[j],']).')


