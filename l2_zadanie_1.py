# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 16:05:21 2018

@author: Rege
"""
'''
Program wyszukuje we wskazanym pliku tekstowym wyrazy podobne do zadanego wzorca za pomocą maksymalnej 
odległości Levenshteina d oraz okresla ich polozenie w tekscie.

argumenty: ["nazwa_pliku_tekstowego", "wzorzec", odległosć_Levenshtein'a] 
                       str               str               int (domyslnie = 1)

Odległosć Levenshtein'a - ilosć zmian jaką należy wprowadzić w jednym wyrazie, aby był taki sam jak inny wyraz.
'''
#Funkcja licząca odległosć Levenshtein'a, źródło:
# https://pl.python.org/forum/index.php?topic=146.0 - temat na forum
# http://hetland.org/coding/python/levenshtein.py - link do pliku z solucją Magnusa Lie Hetlanda

#__początek funkcji__ (odległosć Levenshtein'a)
def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n
        
    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)
            
    return current[n]
#__koniec funkcji__ (odległosć Levenshtein'a)


#__CIAŁO PROGRAMU__:
#dla pliku l2_tekst.txt zalecanymi wzorcamu są: program, użytkownik, sensowny, wołać, kogo, czego

#zaimportuj biblioteki
import sys        #do wprowadzania zmiennych jako argumenty funkcji
import linecache  #do czytania linii w plikach tekstowych
import string     #do obsługi napisów (usuwanie znaków interpunkcyjnych)

#spawdz czy podano odpowiednia ilosc argumentow oraz przypisz wartosci do zmiennych
argumenty=len(sys.argv)
if argumenty == 4:      #jesli podano wszystkie argumenty:
    plik=sys.argv[1]        #nazwa pliku z tekstem 
    wzorzec=sys.argv[2]     #poszukiwany wzorzec wyrazów
    try:
        d=int(sys.argv[3])  #zadana maksymalna odległosć Levenshtein'a
    except ValueError:
        sys.exit("Zadana odleglosc Levenshtein'a musi byc liczba calkowita!")
elif argumenty == 3:    #jesli pominieto ostatni argument:
    plik=sys.argv[1]        
    wzorzec=sys.argv[2]
    d=int(1)
else:                   #w przeciwnym wypadku:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj 2 lub 3 argumenty: [nazwa_pliku_tekstowego, poszukiwany_wzorzec, odleglosc_Levenshteina (domyslnie = 1)]')

#sprawdz czy podana przez uzytkownika nazwa pliku zawiera rozszerzenie
if not plik.lower().endswith(('.txt', '.rtf', '.doc', '.docx', '.odt', '.css', '.html', '.htm', '.xml', '.csv')):    #https://pl.wikipedia.org/wiki/Formaty_plików_tekstowych
    print("\n")
    print('W nazwie pliku nie wykryto rozszerzenia pliku tekstowego. Upewnij sie ze podano odpowiednie rozszerzenie!')
    print('Znane rozszerzenia plikow tekstowych: .txt, .rtf, .doc, .docx, .odt, .css, .html, .htm, .xml, .csv')

#pobierz dane z pliku
try:                        #spróbuj otworzyć plik
    tekst=open(plik,'r')
except:                     #jesli nie uda ci się otworzyć pliku wyswietl błąd i zamknij program
    print("\n")
    print(plik, "\n")
    sys.exit('Nie mozna otworzyc pliku. Upewnij sie ze: \n - wpisano poprawna nazwe pliku \n - nazwa pliku zawiera rozszerzenie \n - plik znajduje sie w folderze zawierajacym kod programu') #zatrzymaj program w tym miejscu i wyswietl błąd
lista=tekst.read()
tekst.close()

print("\n")
print("--------------------------------------------------------------------------------")
print(lista)
print("--------------------------------------------------------------------------------","\n")
print("Poszukiwany wzorzec to [",wzorzec,"], a zadana odleglosc Levenshtein'a wynosi [",d,"]")
print("\n")
print('W pliku',plik,'znaleziono nastepujace wyrazy podobne do wzorca:')

#pętla do sprawdzania i wypisywania wyrazów
petla=1     #nieskonczona petla
linia=1     #licznik linii
wyraz=1     #licznik wyrazu
delete_string=str.maketrans('', '', string.punctuation) #str.maketrans(x,y,z) zamień stringi x na stringi y, a wszystkie znaki interpunkcyjne na "none"

while petla==1:                             #deklaracja nieskończonej pętli
    wiersz=linecache.getline(plik,linia)    #do zmiennej wiersz przypisz kolejny wiersz z pliku (zaczynając od 1)
    if wiersz=="":                          #po natrafieniu na pustą linijke zakończ działanie pętli
        break
    wyrazy=wiersz.lower().split()           #rozdziel wyrazy znakami białymi i zapisz z małych liter jako osobne elementy listy
    for i in range (len(wyrazy)):
        wyrazy[i]=wyrazy[i].translate(delete_string)  #usuń znaki interpunkcyjne z wyrazu
        if levenshtein(wzorzec,wyrazy[i])<=d:         #porównaj kolejne słowa ze wzorcem, jesli pasuje to wyswietl
            print('-',wyrazy[i],'\t( linia',linia,'wyraz',wyraz,')')
        wyraz=wyraz+1
    linia=linia+1
    wyraz=1     #zresetuj licznik wyrazów
    
