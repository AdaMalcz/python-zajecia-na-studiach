# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 14:46:35 2018

@author: Rege
"""

#program tworzy tablicę dwuwymiarową zawierającą tabliczkę mnożenia liczb 
#od 1 do n (gdzie n jest argumentem wywołania programu)

#zaimportuj zmienne systemowe oraz biblioteke math
import sys
import math

#IDIOTOODPORNOŚĆ: n jest liczba dodatnia calkowita
if float(sys.argv[1])-math.floor(float(sys.argv[1]))>0:
    print("\n")
    sys.exit('BLAD: podany wymiar tablicy nie jest liczba calkowita!') #zatrzymaj program w tym miejscu i wyswietl błąd
if float(sys.argv[1])<=0:
    print("\n")
    sys.exit('BLAD: podana wymiar tablicy nie jest liczba dodatnia!') #zatrzymaj program w tym miejscu i wyswietl błąd
    
#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=2:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')
    
n=int(sys.argv[1])  #wymiar tablicy

#zdefiniowanie tablicy zawierającej same zera
tablica = [[0] * n for i in range(n)]

#wypełnienie krawędzi tablicy cyframi od 1 do n
for i in range(n):
    (tablica[0][i])=i+1
for i in range(n):
    (tablica[i][0])=i+1

print("\n")

#wykonanie mnożeń w komórkach tablicy oraz wyswietlanie tablicy w rownych kolumnach o szerokosci 4 znakow
for i in range (0,n):
    line=""
    for j in range (0,n):
        if j>0:
            (tablica[i][j])=(i+1)*(j+1)
        line=line+ ("%4i" % tablica[i][j])
    print (line)

