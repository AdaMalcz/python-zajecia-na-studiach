# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 16:30:45 2018

@author: Rege
"""

#program oblicza n elementów ciągu geometrycznego określonego wzorem:
# a[n+1] = a[n]*q
#dla wyrazu początkowego a[0], ilorazu q oraz liczby obliczanych elementów 
#ciągu n zadanych jako argumenty wejściowe funkcji

#polecane argumenty dla uzyskania wyniku to [1 2 10]
#polecane argumenty dla wyswietelnia powiadomień to [0 2 10], [1 1 10], [1 0 10]
#polecane argumenty dla wywołania błędu to [1 2 0], [1 2 -1], [1 2 1.3]

#zaimportuj zmienne systemowe i biblioteke math
import sys
import math

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=4:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 3 argumenty.')

a0=float(sys.argv[1])
q=float(sys.argv[2])
n=float(sys.argv[3])    #float zamiast int do obsługi błędu

#Powiadomienia dla użytkownika
if a0==0:
    print("\n")
    print ('Pierwszy wyraz ciagu wynosi 0 - wszystkie pozostale wyrazy rowniez wyniosa 0.')
if q==1:
    print("\n")
    print('Iloraz q wynosi 1 - wszystkie wyrazy ciagu beda takie same.')
elif q==0:
    print("\n")
    print('Iloraz ciagu wynosi 0 - wszystkie wyrazy ciagu beda mialy wartosc 0.')

#IDIOTOODPORNOŚĆ (obsługa błędu)
if n-math.floor(n)>0:
    print("\n")
    sys.exit('BLAD: ilosc wyrazow w ciagu musi byc liczba calkowita!') #zatrzymaj program w tym miejscu i wyswietl błąd
elif n<=0:
    print("\n")
    sys.exit('BLAD: ilosc wyrazow musi byc liczba dodatnia!') #zatrzymaj program w tym miejscu i wyswietl błąd
    
n=int(n)    #zamień na int dla licznika pętli for

#zdefiniowanie funkcji geo(wyraz początkowy a[0], iloraz q, liczba obliczanych elementów ciagu)
def geo(a0,q,n):
    ciag=[a0]               #zadeklarowanie zmiennej ciąg o pierwszym wyrazie podanym przez użytkownika
    for i in range (n-1):   #pierwszy wyraz jest już w ciągu, więc dodawanych jest (n-1) kolejnych, aby całosć miała długosć n
        l=ciag[i]*q         #obliczanie kolejnego wyrazu ciągu
        ciag.append(l)      #dodawanie wyrazu na koniec ciągu
    return ciag             #po zakończeniu działania pętli argumentem wyjciowym jest cały ciąg liczb

#wyswietalnie wyniku
print("\n")
print('Obliczono ciag o wzorze: a[n+1] = a[n]*q. Pierwszym wyrazem tego ciagu jest',a0,', a jego iloraz q wynosi',q,"\n")
print('Ciag zawiera',n,'wyrazow:',geo(a0,q,n))