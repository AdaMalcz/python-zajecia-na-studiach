# -*- coding: utf-8 -*-
"""
Created on Wed Nov  7 15:20:36 2018

@author: Rege
"""

#program służy jedynie do wywietlania informacji o poszczególnych programach
#z listy. Zawiera opisy programów, sposoby ich wywołania oraz sugestie
#dotyczące argumentów wejciowych programów w celu przetesgtowania każdej jego
#funkcji
import sys

print("\n")
print('Repozytorium zawiera programy realizujace wszystkie zadania z listy 2 dla Pythona.')
print('Autorem repozytorium jest Adam Malczewski (208470)')
print('Grupa:',"\t",'sroda 15:15',"\n")

argumenty=len(sys.argv)

if argumenty == 1:
    
    print('Zadanie 1:')
    print('Opis:')
    print("\t",'Program wyszukuje we wskazanym pliku tekstowym wyrazy podobne do zadanego wzorca za pomoca maksymalnej odleglosci Levenshteina oraz okresla ich polozenie w tekscie.')
    print("\t", 'Do wyliczenia odleglosci Levenshteina skorzystano z kodu znalezionego w internetach (na forum: https://pl.python.org/forum/index.php?topic=146.0).',"\n")
    print('Uruchomienie:')
    print("\t",'python l2_zadanie_1.py "nazwa_pliku_tekstowego" "wzorzec" odleglosc_Levenshteina (domyslnie = 1)',"\t",'nazwa pliku tekstowego musi zawierac rozszerzenie!',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow')
    print("\t",'- pominiecie ostatniego argumentu ustawi wartosc domyslna = 1')
    print("\t",'- odleglosc Levenshteina podana przez uzytkownika musi byc liczba calkowita')
    print("\t",'- program sprawdza czy nazwa pliku zawiera rozszerzenie')
    print("\t",'- przed rozpoczeciem pracy na pliku program sprawdza czy plik istnieje i czy mozna go otworzyc',"\n")
    print('Zalecane argumenty wywolania:')
    print("\t",'- [l2_tekst.txt program 2] spowoduje znalezienie 3 podobnych slow')
    print("\t",'- [l2_tekst.txt program 2.1] spowoduje wyswietlenie bledu z powodu niecalkowitej wartosci odleglosci Levenshteina')
    print("\t",'- [l2_tekst.txt program] spowoduje ustawienie domyslnej odleglosci Levenshteina na 1 i znalezienie 2 podobnych slow')
    print("\t",'- [l2_tekst program] spowoduje wyswietlenie komunikatu o braku rozszerzenia oraz bledu z powodu nie znalezienia pliku')
    print("\t",'- [l2_tekst.txt] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    #Co jeszcze można zrobić:
    #   napisać własnego Levenshteina ;p
    
    print("\n")
    
    print('Zadanie 2:')
    print('Opis:')
    print("\t",'Program tworzy przykladowe obiekty klasy Wielomian oraz wykorzystuje wszystkie metody wchodzace w sklad tej klasy.') 
    print("\t",'Klase Wielomian napisano w osobnym pliku wielomian.py, obiekty tej klasy to lista z ciagiem wspolczynnikow wielomianu.') 
    print("\t",'Klasa zawiera metody sluzace do wyswietlania, dodawania, odejmowania i mnozenia wielomianow - przeladowano dodatkowo wszystkie operatory zwiazane z tymi metodami.')
    print("\t",'Klasa zawiera tez metody zwracajace stopien wielomianu i wartosc wielomianu dla danego x.') 
    print("\t",'Wykomentowane linijki zawieraja alternatywne obiekty, ktorych stworzenie nie jest mozliwe ze wzgledu na obsluge bledow - aby sprawdzic wywolanie tych bledow nalezy odkomentowac te linijki.',"\n")
    print('Uruchomienie:')
    print("\t",'python l2_zadanie_2.py',"\t",'w ciele programu zaimplementowano 2 przykladowe wielomiany oraz kilka blednych (wykomentowane)',"\n")
    print('Dodatkowe funkcje:')
    print("\t",'- Klasa przeladowuje wszystkie mozliwe operatory')
    print("\t",'- Program wywoluje wszystkie metody klasy oraz wykorzystuje wszystkie przeladowane operatory - zawiera opisy wywolania funkcji')
    print("\t",'- Podane przy tworzeniu obiektu wsplczynniki wielomianu musza byc liczbami calkowitymi')
    print("\t",'- Klasa uniemozliwia stworzenie wielomianu 0 stopnia')
    print("\t",'- Metody do tekstowej reprezentacji wielomianow uwzgledniaja zera i jedynki (nie wyswietlaja zerowych wspolczynnikow a zamiast "1x" wyswietlaja samo "x")')
    print("\t",'- Metody do tekstowej reprezentacji wielomianow uwzgledniaja wartosci ujemne (wyswietlaja "-" zamiast "+" przed ujemnymi wspolczynnikami)',"\n")
    #Co jeszcze można zrobić:
    #   wiecej dzialan na wielomianach
    
    
 

elif argumenty == 2:
    zadanie=sys.argv[1]
    
    if zadanie == "1":
        print('Zadanie 1:')
        print('Opis:')
        print("\t",'Program wyszukuje we wskazanym pliku tekstowym wyrazy podobne do zadanego wzorca za pomoca maksymalnej odleglosci Levenshteina oraz okresla ich polozenie w tekscie.')
        print("\t", 'Do wyliczenia odleglosci Levenshteina skorzystano z kodu znalezionego w internetach (na forum: https://pl.python.org/forum/index.php?topic=146.0).',"\n")
        print('Uruchomienie:')
        print("\t",'python l2_zadanie_1.py "nazwa_pliku_tekstowego" "wzorzec" odleglosc_Levenshteina (domyslnie = 1)',"\t",'nazwa pliku tekstowego musi zawierac rozszerzenie!',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- program nie zadziala przy podaniu niewlasciwej ilosci argumentow')
        print("\t",'- pominiecie ostatniego argumentu ustawi wartosc domyslna = 1')
        print("\t",'- odleglosc Levenshteina podana przez uzytkownika musi byc liczba calkowita')
        print("\t",'- program sprawdza czy nazwa pliku zawiera rozszerzenie')
        print("\t",'- przed rozpoczeciem pracy na pliku program sprawdza czy plik istnieje i czy mozna go otworzyc',"\n")
        print('Zalecane argumenty wywolania:')
        print("\t",'- [l2_tekst.txt program 2] spowoduje znalezienie 3 podobnych slow')
        print("\t",'- [l2_tekst.txt program 2.1] spowoduje wyswietlenie bledu z powodu niecalkowitej wartosci odleglosci Levenshteina')
        print("\t",'- [l2_tekst.txt program] spowoduje ustawienie domyslnej odleglosci Levenshteina na 1 i znalezienie 2 podobnych slow')
        print("\t",'- [l2_tekst program] spowoduje wyswietlenie komunikatu o braku rozszerzenia oraz bledu z powodu nie znalezienia pliku')
        print("\t",'- [l2_tekst.txt] spowoduje wyswietlenie bledu z powodu niewlasciwej liczby argumentow',"\n")
    
    elif zadanie == "2":
        print('Zadanie 2:')
        print('Opis:')
        print("\t",'Program tworzy przykladowe obiekty klasy Wielomian oraz wykorzystuje wszystkie metody wchodzace w sklad tej klasy.') 
        print("\t",'Klase Wielomian napisano w osobnym pliku wielomian.py, obiekty tej klasy to lista z ciagiem wspolczynnikow wielomianu.') 
        print("\t",'Klasa zawiera metody sluzace do wyswietlania, dodawania, odejmowania i mnozenia wielomianow - przeladowano dodatkowo wszystkie operatory zwiazane z tymi metodami.')
        print("\t",'Klasa zawiera tez metody zwracajace stopien wielomianu i wartosc wielomianu dla danego x.') 
        print("\t",'Wykomentowane linijki zawieraja alternatywne obiekty, ktorych stworzenie nie jest mozliwe ze wzgledu na obsluge bledow - aby sprawdzic wywolanie tych bledow nalezy odkomentowac te linijki.',"\n")
        print('Uruchomienie:')
        print("\t",'python l2_zadanie_2.py',"\t",'w ciele programu zaimplementowano 2 przykladowe wielomiany oraz kilka blednych (wykomentowane)',"\n")
        print('Dodatkowe funkcje:')
        print("\t",'- Klasa przeladowuje wszystkie mozliwe operatory')
        print("\t",'- Program wywoluje wszystkie metody klasy oraz wykorzystuje wszystkie przeladowane operatory - zawiera opisy wywolania funkcji')
        print("\t",'- Podane przy tworzeniu obiektu wsplczynniki wielomianu musza byc liczbami calkowitymi')
        print("\t",'- Klasa uniemozliwia stworzenie wielomianu 0 stopnia')
        print("\t",'- Metody do tekstowej reprezentacji wielomianow uwzgledniaja zera i jedynki (nie wyswietlaja zerowych wspolczynnikow a zamiast "1x" wyswietlaja samo "x")')
        print("\t",'- Metody do tekstowej reprezentacji wielomianow uwzgledniaja wartosci ujemne (wyswietlaja "-" zamiast "+" przed ujemnymi wspolczynnikami)',"\n")

   
    else:
        sys.exit('Nie ma takiego zadania.')

else:
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')
    
 