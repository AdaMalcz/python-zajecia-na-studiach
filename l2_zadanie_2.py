# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 02:28:03 2018

@author: Rege
"""

'''
Program tworzy przykladowe obiekty klasy Wielomian oraz wykorzystuje wszystkie
metody wchodzące w skład tej klasy. Wykomentowane linijki zawierają alternatywne 
obiekty, których stworzenie nie jest możliwe ze względu na obsługę błędów - aby
sprawdzić wywołanie tych błędów należy odkomentować te linijki
'''

"Importowanie klasy Wielomian"
from wielomian import Wielomian

"Utworzenie przykładowych obiektów klasy Wielomian"
w1=Wielomian([1,2,3])
#w1=Wielomian([1])
#w1=Wielomian([1, 2, "trzy"])
#w1=Wielomian([1, 2, 3.1])
w2=Wielomian([4,5])
print("\n")

"Wyswietlenie wielomianu na 3 sposoby"
print('Pierwszy wielomian: \t (wyswietlono przy pomocy klasycznej metody)')
print('\t >> w1.Wprint()')
w1.Wprint()
print("\n")
print('Drugi wielomian: \t (wyswietlono przy pomocy metody __str__)')
print('\t >> print(w2)')
print(w2)
print("\n")
print('Drugi wielomian: \t (wyswietlono przy pomocy metody __str__)')
print("\t >> print('Oto drugi wielomian: ' + str(w2))")
print('Oto drugi wielomian: ' + str(w2))
print("\n")

"Zwracanie stopnia wielomianu"
print('Wielomian [' + str(w1) + '] jest wielomanem stopnia',str(w1.stopien()) + '.')
print('Wielomian [' + str(w2) + '] jest wielomanem stopnia',str(w2.stopien()) + '.')
print("\n")

"Zwracanie wartosci wielomianu W(x)"
x=3
y=4
print('Wartosc wielomianu [' + str(w1) + '] przy x=' + str(x) + ' wynosi ' + str(w1.wartosc(x)))
print('Wartosc wielomianu [' + str(w2) + '] przy x=' + str(y) + ' wynosi ' + str(w2.wartosc(y)))
print("\n")

"Dodawanie wielomianów"
print('Dodawanie przy pomocy klasycznej metody: \t [w1.dodaj(w2)]')
print('['+ str(w1) +']', '+' ,'[' + str(w2) + ']', '=','['+str(w1.dodaj(w2))+']')
print('Dodawanie przy pomocy metody __add__: \t\t [w1+w2]')
print('['+ str(w1) +']', '+' ,'[' + str(w2) + ']', '=','['+str(w1+w2)+']')
print("\n")

"Odejmowanie wielomianów"
print('Odejmowanie przy pomocy klasycznej metody: \t [w1.odejmij(w2)]')
print('['+ str(w1) +']', '-' ,'[' + str(w2) + ']', '=','['+str(w1.odejmij(w2))+']')
print('Odejmiwanie przy pomocy metody __sub__: \t [w1-w2]')
print('['+ str(w1) +']', '-' ,'[' + str(w2) + ']', '=','['+str(w1-w2)+']')
print("\n")

"Mnożenie wielomianów"
print('Mnozenie przy pomocy klasycznej metody: \t [w1.mnozenie(w2)]')
print('['+ str(w1) +']', '*' ,'[' + str(w2) + ']', '=','['+str(w1.mnozenie(w2))+']')
print('Mnozenie przy pomocy metody __mul__:  \t\t [w1*w2]')
print('['+ str(w1) +']', '*' ,'[' + str(w2) + ']', '=','['+str(w1*w2)+']')
