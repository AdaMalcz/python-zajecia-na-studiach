# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 19:33:36 2018

@author: Rege
"""

#ulepszona wersja programu z zadania 7
#program umożliwia wprowadzanie wyrazu w pętli, komendy:
#[quit]= kończy działanie programu
#[read]= wyswietla zawartosć pliku tekstowego
#[clear]= czysci plik tekstowy

import sys

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=2:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')

nazwa=sys.argv[1]

print('Program zliczy sylaby w podanym wyrazie oraz zapisze dane do pliku',nazwa,'. Znane sylaby to: [a] [e] [i] [o] [u] [y]',"\n")
print('Jesli chcesz wyswietlic zawartosc listy zamiast wyrazu wpisz: read')
print('Jesli chcesz wyczyscic zawartosc listy zamiast wyrazu wpisz:  clear')
print('Jesli chcesz wyjsc z programu zamiast wyrazu wpisz:           quit')
print("\n")

status="on"

while status=="on":
    wyraz = input("Podaj wyraz:")
    if wyraz == "quit":
        status="off"
        break
    elif wyraz == "read":
        czytaj=open(nazwa,'r').read()
        print(czytaj)
        print("\n")
    elif wyraz == "clear":
        czysc=open(nazwa,'w') #a - append
        czysc.write("")
        czysc.close()
        print('Wyczyszczono plik',nazwa,'.')
    else:
        a=wyraz.count('a')
        e=wyraz.count('e')
        i=wyraz.count('i')
        o=wyraz.count('o')
        u=wyraz.count('u')
        y=wyraz.count('y')
        
        print('Podano wyraz:',wyraz,"\n")
        if a>0:
            print('W wyrazie zanjduje sie',a,'samoglosek a.')
        if e>0:
            print('W wyrazie zanjduje sie',e,'samoglosek e.')
        if i>0:
            print('W wyrazie zanjduje sie',i,'samoglosek i.')
        if o>0:
            print('W wyrazie zanjduje sie',o,'samoglosek o.')
        if u>0:
            print('W wyrazie zanjduje sie',u,'samoglosek u.')
        if y>0:
            print('W wyrazie zanjduje sie',y,'samoglosek y.')
        suma=a+e+i+o+u+y
        print("\n")
        print('W sumie w wyrazie znajduje sie',suma,'samoglosek.',"\n")
        
        lista=[wyraz,"\t",str(suma),"\n"]
        plik=open(nazwa,'a') #a - append
        plik.writelines(lista)
        plik.close()
    
        print('Wyraz oraz sume zliczonych samoglosek dopisano na koniec pliku',nazwa,'w folderze zawierajacym kod programu.',"\n","\n")

print('Zakonczono prace programu.')
