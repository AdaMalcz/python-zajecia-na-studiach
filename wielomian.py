# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 14:51:36 2018

@author: Rege
"""
import sys    #do sys.exit
import copy   #do deep copy
from math import fabs as mod  #wartosc bezwzgledna w print ; math.fabs(-1) = 1 ==> mod(-1) = 1 

#__początek KLASY__ (Wielomian)
class Wielomian(object):    #deklaracja klasy ; [object] - konstruktor klasy
    '''
    Klasa reprezentuje wielomian dowolnego stopnia i pozwala wyswietlić
    wielomiany oraz wykonać na nich typowe operacje matematyczne.
    
    [object] - lista współczynników wielomianu (założenie: liczby całkowite)
    '''
    
    def __init__(self, a):  #tworzenie obiektu klasy Wielomian [autowywołanie]
        self.a=a
        if len(a)>=2:
            for i in range (len(a)):
                if type(self.a[i]) != int:
                    sys.exit('Wspolczynniki wielomianu musza byc liczbami calkowitymi.')
                
                ''' starsza metoda (działa tak samo, ale zajmuje więcej miejsca)
                try:
                    self.a[i]=int(str(a[i]))    #str(a[i]), bo gdyby zostawić samo a[i] zapisze liczby niecalkowite (ucinajac po przecinku)
                except ValueError:
                    sys.exit('Wspolczynniki wielomianu musza byc liczbami calkowitymi.')
                '''
            self.n=(len(a)-1)
        else:
            sys.exit('Podaj conajmniej dwa wspolczynniki wielomianu (nie mozna utworzyc wielomianu 0 stopnia).')
    
    def stopien(self):  #metoda zwracajaca stopien wielomianu
        return self.n
    
    def Wprint(self):  #meotda zwracająca reprezentację tekstową wielomianu
        x=self.n
        if self.a[0]<0:
            print('-', end=" ")
        for i in range (len(self.a)-2):
            if self.a[i]!=1 and self.a[i]!=0:
                print(str(int(mod(self.a[i]))) + "x^" + str(x), end=" ")
                x=x-1
            elif self.a[i]==1 or self.a[i]==-1:
                print("x^" + str(x), end=" ")
                x=x-1
            if self.a[i]!=0 and self.a[i+1]!=0:
                if self.a[i+1]>0:
                    print('+',end=" ")
                else:
                    print('-', end=" ")
        if self.a[-2]!=0:
            print(str(int(mod(self.a[-2]))) + "x", end=" ")
            
        if self.a[-1]!=0:
            if self.a[-1]>0:
                print('+ ' + str(int(mod(self.a[-1]))))
            else:
                print('- ' + str(int(mod(self.a[-1]))))
        
    def __str__(self): #meotda zwracająca reprezentację tekstową wielomianu [autowywołanie poprzez print() lub str()]
        tekst=""
        x=self.n
        if self.a[0]<0:
            tekst+=('- ')
        for i in range (len(self.a)-2):
            if self.a[i]!=1 and self.a[i]!=0:
                tekst+=(str(int(mod(self.a[i]))) + "x^" + str(x))
                x=x-1
            elif self.a[i]==1 or self.a[i]==-1:
                tekst+=("x^" + str(x))
                x=x-1
            if self.a[i]!=0 and self.a[i+1]!=0:
                if self.a[i+1]>0:
                    tekst+=(' + ')
                else:
                    tekst+=(' - ')
        if self.a[-2]!=0:
            tekst+=(str(int(mod(self.a[-2]))) +'x')
        if self.a[-1]!=0:
            if self.a[-1]>0:
                tekst+=(' + ' + str(int(mod(self.a[-1]))))
            else:
                tekst+=(' - ' + str(int(mod(self.a[-1]))))
        return (tekst)
    
    def wartosc(self,x): #metoda zwracajaca wartosc wielomianu dla zadanej wartosci paramtetru x
        wynik=0
        n=self.n
        for i in range (len(self.a)):
            wynik=wynik+(self.a[i]*(x**n))
            n-=1
        return (wynik)
       
    def dodaj(self,other): #metoda wykonujaca dodawanie wielomianow
        wynik=[]
        a=copy.deepcopy(self.a)
        b=copy.deepcopy(other.a)
        while len(a) != len(b):
            if len(a)-len(b)>0:
                b.insert(0, 0)      #array.insert(index, value) - na miejsce 0 wstaw wartosc 0
            else:
                a.insert(0, 0)
        for i in range (len(a)):
            wspl = a[i]+b[i]
            wynik.append(wspl)
        return Wielomian(wynik)
    
    def __add__(self,other): ##metoda wykonujaca dodawanie wielomianow [autowywolywanie przez +]
        wynik=[]
        a=copy.deepcopy(self.a)
        b=copy.deepcopy(other.a)
        while len(a) != len(b):
            if len(a)-len(b)>0:
                b.insert(0, 0)      #array.insert(index, value) - na miejsce 0 wstaw wartosc 0
            else:
                a.insert(0, 0)
        for i in range (len(a)):
            wspl = a[i]+b[i]
            wynik.append(wspl)
        return Wielomian(wynik)

    def odejmij(self,other): #metoda wykonujaca odejmowanie wielomianow
        wynik=[]
        a=copy.deepcopy(self.a)
        b=copy.deepcopy(other.a)
        while len(a) != len(b):
            if len(a)-len(b)>0:
                b.insert(0, 0)      #array.insert(index, value) - na miejsce 0 wstaw wartosc 0
            else:
                a.insert(0, 0)
        for i in range (len(a)):
            wspl = a[i]-b[i]
            wynik.append(wspl)
        return Wielomian(wynik)
    
    def __sub__(self,other): #metoda wykonujaca odejmowanie wielomianow [autowywolywanie przez -]
        wynik=[]
        a=copy.deepcopy(self.a)
        b=copy.deepcopy(other.a)
        while len(a) != len(b):
            if len(a)-len(b)>0:
                b.insert(0, 0)      #array.insert(index, value) - w tablicy '"array" na miejsce 'index' wstaw wartosc 'value'
            else:
                a.insert(0, 0)
        for i in range (len(a)):
            wspl = a[i]-b[i]
            wynik.append(wspl)
        return Wielomian(wynik)
    
    def mnozenie(self,other): #metoda wykonujaca mnożenie wielomianów
        wynik=[]
        tablica=[]
        n1=len(self.a)-1
        n2=len(other.a)-1
        nw=n1+n2
        suma=0

        for i in range (len(self.a)):           #tworzenie tablicy z elementami nieposumowanego ciągu
            for j in range (len(other.a)):
                wspl=self.a[i]*other.a[j]
                n=n1+n2
                element=[wspl, n]
                tablica.append(element)
                n2-=1
            n2=len(other.a)-1 
            n1-=1
        
        while nw != -1:                         #sumowanie elementów wyjsciowego ciągu
            for i in range (len(tablica)):
                if tablica[i][1] == nw:
                    suma+=tablica[i][0]
            wynik.append(suma)
            nw-=1
            suma=0

        return Wielomian(wynik)

    def __mul__(self,other): #metoda wykonujaca mnożenie wielomianów [autowywolywanie przez *]
        wynik=[]
        tablica=[]
        n1=len(self.a)-1
        n2=len(other.a)-1
        nw=n1+n2
        suma=0

        for i in range (len(self.a)):
            for j in range (len(other.a)):
                wspl=self.a[i]*other.a[j]
                n=n1+n2
                element=[wspl, n]
                tablica.append(element)
                n2-=1
            n2=len(other.a)-1 
            n1-=1
        
        while nw != -1:
            for i in range (len(tablica)):
                if tablica[i][1] == nw:
                    suma+=tablica[i][0]
            wynik.append(suma)
            nw-=1
            suma=0

        return Wielomian(wynik)        
#__koniec KLASY__ (Wielomian)