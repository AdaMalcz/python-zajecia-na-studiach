# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 16:52:58 2018

@author: Rege
"""

#program wczytuje plik tekstowy utworzony w zadaniu 7 (nazwa pliku argumentem
#wywołania) oraz wyswietla posortowane według liczby samogłosek wyrazy

#zaimportuj biblioteki i przypisz pierwszy argument wywołania do zmiennej
import sys
import linecache

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=2:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')

nazwa=sys.argv[1]   #nazwa pliku do posortowania

#pobierz dane z pliku
try:                        #spróbuj otworzyć plik
    plik=open(nazwa,'r')
except:                     #jesli nie uda ci się otworzyć pliku wyswietl błąd i zamknij program
    print("\n")
    sys.exit('Nie mozna otworzyc pliku. Upewnij sie ze wpisano poprawna nazwe pliku wraz z jego rozszerzeniem oraz ze plik znajduje sie w folderze zawierajacym kod programu.') #zatrzymaj program w tym miejscu i wyswietl błąd
lista=plik.read()
plik.close()

#wyswietl komunikat o funkcji programu
print("\n")
print('Program pobral dane z pliku',nazwa,'.')

#wyswietl nieposortowane dane
print('W pliku',nazwa,'znajduje sie nastepujaca lista wyrazow wraz z okreslona liczba samoglosek:',"\n")
print(lista,"\n")

#deklaracja zmiennych
petla=1     #nieskonczona petla
i=0         #licznik wyrazów
sort=[]     #lista wyrazów do posortowania

#zapis wyrazów i zmiennej sortującej do listy
while petla==1:                           #deklaracja nieskończonej pętli
    wiersz=linecache.getline(nazwa,i+1)   #do zmiennej wiersz przypisz kolejny wiersz z pliku (zaczynając od 1)
    if wiersz=="":                        #po natrafieniu na pustą linijke zakończ działanie pętli
        break
    else:
        wierszx=wiersz.split("\t")        #rozdziel stringa (separator: tab) i przypisz do zmiennej
        wierszx[1]=int(wierszx[1])        #zamień drugiego stringa w ciagu na inta
        i=i+1                             #zwieksz licznik wyrazów
        sort.append(wierszx)              #dodaj zmienną do listy

#wyswietl komunikat o funkcji programu
print('W sumie w pliku',nazwa,'znajduje sie',i,'wyrazow.',"\n")   

#posortuj listę na podstawie drugiego argumentu
posortowane=sorted(sort, key=lambda sortuj: sortuj[1]) #lambda to anonimowa funkcja wywolana w locie bez definiowania, istnieje tylko podczas procesu sortowania  

#wyswietl komunikat o funkcji programu
print('Lista wyrazow posortowanych pod wzgledem ilosci samoglosek (rosnaco):')
for j in range(i):
    print(posortowane[j][0])