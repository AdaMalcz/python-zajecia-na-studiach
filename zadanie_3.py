# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 20:44:46 2018

@author: Rege
"""

#program oblicza sumę kwadratów liczb od 1 do n na podstawie własnej funkcji,
#umożliwiając użytkownikowi podanie argumentu n tej funkcji
#            [wynik = 1^2 + 2^2 + ... + n^2]
#polecany argument dla uzyskania wyniku to [4] (1+4+9+16 = 30)
#polecane argumenty dla wywołania błędu to [0] [-1] oraz [1.1] 

#zaimportuj zmienne systemowe i bibliotekę math
import sys
import math

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=2:
    print("\n")
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 1 argument.')
    
#przypisz argumenty wywołania do zmiennych
a=float(sys.argv[1])

print("\n")

#IDIOTOODPORNOŚĆ: n jest nieujemną liczbą całkowitą
if a<1:
    sys.exit('BLAD: podany argument jest liczba mniejsza od 1. 1 jest pierwszym wyrazem sumowanego ciagu!') #zatrzymaj program w tym miejscu i wyswietl błąd
elif a-math.floor(a)>0:
    sys.exit('BLAD: podany argument nie jest liczba calkowita!') #zatrzymaj program w tym miejscu i wyswietl błąd

print('Program obliczy sume wszystkich wyrazow ciagu [1^2, 2^2, ..., n^2], dla n=',int(a))

#zdefinuj własną funkcję sumakwadratow(n)
def sumakwadratow(n):
    wynik=0 #zdefiniuj zmienną wynik
    for x in range(n):  #licznik x zmienny w przedziale od 0 do n-1; dla n pętla STOP
        wynik = wynik + (x+1)**2
    return wynik

#wywołaj funkcję wynik z argumentem a
wynik=sumakwadratow(int(a)) #koniec zakresu licznika musi być liczbą całkowitą
print('Suma kwadratow liczb od 1 do',int(a),'wynosi',int(wynik))
