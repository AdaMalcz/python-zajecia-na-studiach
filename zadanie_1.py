# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 16:21:04 2018

@author: Rege
"""
#program oblicza pole trójkąta o długości boków podanej przez użytkownika 
#polecane argumenty dla uzyskania wyniku to [2 2 3]
#polecane argumenty dla wywołania błędu to [1 2 3]

#[sys] System-specific parameters and functions
#This module provides access to some variables used or maintained by the 
#interpreter and to functions that interact strongly with the interpreter. 
#It is always available

#zaimportuj zmienne systemowe oraz biblioteke math (funkcje matematyczne)
import sys 
import math #wywołanie funkcji odbywa się poprzez math.[nazwa fuknkcji]

#spawdz czy podano odpowiednia ilosc argumentow
argumenty=len(sys.argv)
if argumenty !=4:
    sys.exit('Niepoprawna ilosc argumentow. Podaj dokladnie 3 argumenty.')
    
#przypisz do zmiennych kolejne argumenty wywołania programu
#argv[0] to scieżka dostępu do programu
#np: [będąc w folderze] python zadanie_1.py 1 2 3
#print(a+b+c) => 123
a=float(sys.argv[1]) #float bo: 
b=float(sys.argv[2]) # "unsupported operand type(s) for /: 'str' and 'int'" 
c=float(sys.argv[3]) #w konsoli po wywołaniu

#IDIOTOODPORNOŚĆ: zasada tworzenia trójkątów:
#długość jednego boku musi być mniejsza niż suma długości dwóch pozostałych boków
if (a+b)<=c:
    print ('BLAD: dlugosc jednego boku musi byc mniejsza niz suma dlugosci dwoch pozostalych bokow')
elif (b+c)<=a:
    print ('BLAD: dlugosc jednego boku musi byc mniejsza niz suma dlugosci dwoch pozostalych bokow')
elif (c+a)<=b:
    print ('BLAD: dlugosc jednego boku musi byc mniejsza niz suma dlugosci dwoch pozostalych bokow')
else:   #jesli sie zgadza policz i wyswietl pole
    p=(a+b+c)/2
    P=math.sqrt(p*(p-a)*(p-b)*(p-c))
    print ('Pole trojkata o bokach ',a,', ',b, ', ',c, ' wynosi ',P)
    
#w Pythonie bloki zdefioniowane są przez wcięcia
#docelowo 4 spacje, ale liczy się konsekwencja

